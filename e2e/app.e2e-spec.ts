import { GiphyTestPage } from './app.po';

describe('giphy-test App', () => {
  let page: GiphyTestPage;

  beforeEach(() => {
    page = new GiphyTestPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
