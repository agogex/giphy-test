import { Injectable } from '@angular/core';
import { Subject } from 'rxjs/Subject';

@Injectable()
export class CommunicationService {
  private searchResultSource = new Subject();
  private paginationSource = new Subject();

  searchResult$ = this.searchResultSource.asObservable();
  pagination$ = this.paginationSource.asObservable();

  searchResultEmit(result) {
    this.searchResultSource.next(result);
  }

  paginationEmit(page) {
    this.paginationSource.next(page);
  }
}