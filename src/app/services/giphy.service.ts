import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';

import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';

const GIPHY_API         = 'https://api.giphy.com/v1/gifs';
const GIPHY_API_UPLOAD  = 'https://upload.giphy.com/v1/gifs';
const API_KEY           = 'dc6zaTOxFJmzC';
export const PAGE_LIMIT = 12;

@Injectable()
export class GiphyService {
    constructor(private http: Http) {}

    getTrending()  {
        return this.http.get(`${GIPHY_API}/trending?api_key=${API_KEY}&limit=${PAGE_LIMIT}`)
            .map(res => {
                return res.json();
            });
    }

    searchGiphy(query: string, page: number = 0) {
        return this.http.get(`${GIPHY_API}/search?q=${query}&api_key=${API_KEY}&limit=${PAGE_LIMIT}&offset=${page}`)
            .map(res => res.json());
    }

    searchGiphyById(ids: string,  page: number = 0) {
        return this.http.get(`${GIPHY_API}?ids=${ids}&api_key=${API_KEY}&limit=${PAGE_LIMIT}&offset=${page}`)
            .map(res => res.json());
    }

    midifyCollection(id: string) {
        let collection: any = this.getfromLocalStorage('collection');
        const index = collection.indexOf(id);
        let inCollection;
        if ( index === -1) {
            collection = collection.concat([id]);
            inCollection = true;
        } else {
            collection.splice(index, 1);
            inCollection = false;
        }
        window.localStorage.setItem('collection', JSON.stringify(collection));
        return inCollection;
    }

    getfromLocalStorage(item: string) {
        return JSON.parse(window.localStorage.getItem(item)) || [];
    }

    processCollection(data) {
        const collection: any = this.getfromLocalStorage('collection');
        data.forEach( (item, i, arr) => {
            if(collection.indexOf(item.id) === -1) {
                item.inCollection = false;
            } else {
                item.inCollection = true;
            }
        });
        return data;
    }

    uploadImage(data) {
        data.append('api_key', API_KEY);
        return this.http.post(`${GIPHY_API_UPLOAD}`, data)
            .map(res => res.json());
    }
}