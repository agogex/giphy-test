import { Routes, RouterModule } from '@angular/router';

// Components
import { HomeComponent } from './components/home/home.component';
import { MyCollectionComponent } from './components/my-collection/my-collection.component';
import { UploadComponent } from './components/upload/upload.component';

import { HomeResolveService } from './components/home/home-resolve.service';

const routes: Routes = [
    {
        path: '',
        component: HomeComponent,
        resolve: {
            images: HomeResolveService
        }
    },
    {
        path: 'my-collection',
        component: MyCollectionComponent
    },
    {
        path: 'upload',
        component: UploadComponent
    }
];

export const appRouting = RouterModule.forRoot(routes);