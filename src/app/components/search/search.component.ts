import { Component, EventEmitter, Output } from '@angular/core';

import { CommunicationService } from '../../services/communication.service';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.scss']
})

export class SearchComponent {
  @Output() search: EventEmitter<any>;

  constructor(private cs: CommunicationService) {
    this.search = new EventEmitter();
  }

  searchHandler(query) {
    this.cs.searchResultEmit(query);
  }
}