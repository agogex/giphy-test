import { Component, OnInit } from '@angular/core';

import { GiphyService } from '../../services/giphy.service';

@Component({
    selector: 'app-my-collection',
    templateUrl: './my-collection.component.html',
    styleUrls: ['./my-collection.component.scss']
})
export class MyCollectionComponent implements OnInit {
    images;

    constructor(private giphyService: GiphyService) {}

    ngOnInit() {
        this.getCollection();
    }

    getCollection() {
        let ids = this.giphyService.getfromLocalStorage('collection').join(',');
        this.giphyService.searchGiphyById(ids)
            .subscribe(res => this.images = this.giphyService.processCollection(res.data));
    }
}