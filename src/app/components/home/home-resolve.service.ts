import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot } from '@angular/router';

import { GiphyService } from '../../services/giphy.service';

@Injectable()
export class HomeResolveService implements Resolve<any> {
  constructor(private giphyService: GiphyService) {}

  resolve() {
    return this.giphyService.getTrending();
  }
}