import { Component, OnInit, Input } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { GiphyService, PAGE_LIMIT } from '../../services/giphy.service';
import { CommunicationService } from '../../services/communication.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
})
export class HomeComponent implements OnInit {
  @Input() images: Object[];
  query: string;
  pagination = { 
    limit:       PAGE_LIMIT,
    total_count: 0,
    count:       0,
    offset:      0
  };

  constructor(
    private giphyService: GiphyService,
    private route:        ActivatedRoute,
    private cs:           CommunicationService
  ) {
    cs.searchResult$.subscribe((query: string) => {
      this.query = query;
      this.giphyService
        .searchGiphy(query)
        .subscribe(res => this.processResult.call(this, res));
    });
  }

  ngOnInit() {
    this.route.data.subscribe(data => this.images = data.images.data);
  }
  
  processResult(res) {
    res.pagination.limit = PAGE_LIMIT;
    this.pagination = res.pagination;
    this.images     = this.giphyService.processCollection(res.data);
  }

  addToCollection(image) {
    image.inCollection = this.giphyService.midifyCollection(image.id);
  }

  onNext() {
    this.giphyService
      .searchGiphy(this.query, this.pagination.offset + this.pagination.limit)
      .subscribe(res => this.processResult.call(this, res));
  }

  onPrev() {
    this.giphyService
      .searchGiphy(this.query, this.pagination.offset - this.pagination.limit)
      .subscribe(res => this.processResult.call(this, res));
  }

}
