import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';

import { GiphyService } from '../../services/giphy.service';

@Component({
    selector: 'app-upload',
    templateUrl: './upload.component.html',
    styleUrls: ['./upload.component.scss']
})
export class UploadComponent implements OnInit {
    form: FormGroup;
    fileLength: number = 0;
    usernameError: string;
    fileError: string;

    constructor(
        private fb: FormBuilder,
        private giphyService: GiphyService,
        private router: Router
    ) { }

    ngOnInit() {
        this.formBuild();
    }

    formBuild() {
        this.form = this.fb.group({
            username: [''],
            file:     [''],
            tags:     ['']
        });
    }

    onChange(event) {
        this.fileLength = event.target.value.length;
    }

    onSubmit(event) {
        let username = this.form.get('username');
        let file     = this.form.get('file');
        
        this.fileError     = '';
        this.usernameError = '';

        if (username.invalid) {

            this.usernameError = 'Username is required'

        } else if (file.invalid && this.fileLength === 0) {

            this.fileError = 'File is required'

        } else {

            let upload = new FormData(event.target);
            this.giphyService.uploadImage(upload).subscribe(res => {
                if (res.meta.msg === "OK") {
                    this.giphyService.midifyCollection(res.data.id);
                    this.router.navigate(['my-collection']);
                } else {
                    console.log(res.meta.msg);
                }
            });
        }

        // let upload = new FormData(event.target);
        // this.giphyService.uploadImage(upload).subscribe(res => {
        //     if (res.meta.msg === "OK") {
        //         this.giphyService.midifyCollection(res.data.id);
        //         this.router.navigate(['my-collection']);
        //     } else {
        //         console.log(res.meta.msg);
        //     }
        // });
    }

}