import { Component, Input, EventEmitter, Output } from '@angular/core';

import { GiphyService } from '../../services/giphy.service';
import { DialogService } from '../dialog/dialog.service';

@Component({
    selector: 'app-card',
    templateUrl: './card.component.html',
    styleUrls: ['./card.component.scss']
})
export class CardComponent {
    @Input() images;

    @Output() change: EventEmitter<any> = new EventEmitter();

    constructor(
        private giphyService: GiphyService,
        private dialog: DialogService
    ) { }

    addToCollection(image) {
        image.inCollection = this.giphyService.midifyCollection(image.id);
        this.change.emit();
    }

    openDialog(image) {
        this.dialog.getDialog(image.images.original.url);
    }
}