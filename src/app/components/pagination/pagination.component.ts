import { Component, Input, Output, EventEmitter, OnChanges, ChangeDetectionStrategy } from '@angular/core';

@Component({
    selector: 'pagination',
    templateUrl: './pagination.component.html',
    styleUrls: ['./pagination.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class PaginationComponent implements OnChanges {
    @Input() pagination;
    
    @Output() next = new EventEmitter<any>();
    @Output() prev = new EventEmitter<any>();

    isPrev: boolean;
    isNext: boolean;

    ngOnChanges(changes)  {
        this.isPrev = this.pagination.offset > 0;
        this.isNext = (this.pagination.total_count - this.pagination.offset) >= this.pagination.limit;
    }

    handlePrev() {
        this.prev.emit();
    }

    handleNext() {
        this.next.emit();
    }
}