import { Injectable } from '@angular/core';
import { MdDialogRef, MdDialog, MdDialogConfig } from '@angular/material';

import { DialogComponent } from './dialog.component';

@Injectable()
export class DialogService {
    constructor(private dialog: MdDialog) { }

    getDialog(content: string) {
        let dialogRef: MdDialogRef<DialogComponent>;

        dialogRef = this.dialog.open(DialogComponent);
        dialogRef.componentInstance.content = content;

        return dialogRef.afterClosed();
    }
}