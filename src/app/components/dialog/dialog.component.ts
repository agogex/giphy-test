import { Component, Input } from '@angular/core';
import { MdDialogRef } from '@angular/material';

@Component({
    selector: 'card-dialog',
    templateUrl: './dialog.component.html',
    styleUrls: ['dialog.component.scss']
})
export class DialogComponent {
    @Input() content: string;

    constructor(public dialogRef: MdDialogRef<DialogComponent>) {}
}