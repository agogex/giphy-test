// Angular modules
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpModule } from '@angular/http';
import { ReactiveFormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MdCardModule, MdDialogModule, MdButtonModule } from '@angular/material';

// Routing
import { appRouting } from './app.routing';

// App Components
import { AppComponent } from './app.component';
import { HomeComponent } from './components/home/home.component';
import { MyCollectionComponent} from './components/my-collection/my-collection.component';
import { CardComponent } from './components/card/card.component';
import { UploadComponent } from './components/upload/upload.component';
import { PaginationComponent } from './components/pagination/pagination.component';
import { DialogComponent } from './components/dialog/dialog.component';
import { SearchComponent } from './components/search/search.component';
import { NavigationComponent } from './components/navigation/navigation.component';

// Services
import { GiphyService } from './services/giphy.service';
import { DialogService } from './components/dialog/dialog.service';
import { HomeResolveService } from './components/home/home-resolve.service';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    MyCollectionComponent,
    CardComponent,
    UploadComponent,
    PaginationComponent,
    DialogComponent,
    SearchComponent,
    NavigationComponent
  ],
  imports: [
    BrowserModule,
    HttpModule,
    ReactiveFormsModule,
    BrowserAnimationsModule,
    MdCardModule,
    MdDialogModule,
    MdButtonModule,
    appRouting
  ],
  providers: [
    GiphyService,
    DialogService,
    HomeResolveService
  ],
  entryComponents: [DialogComponent],
  bootstrap: [AppComponent]
})
export class AppModule { }
