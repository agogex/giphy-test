import { Component } from '@angular/core';
import { GiphyService } from './services/giphy.service';
import { CommunicationService } from './services/communication.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
  providers: [CommunicationService]
})
export class AppComponent {

  images: Object[];

  constructor(
    private giphyService: GiphyService,

  ) { }

  getImages() {
    this.giphyService.getTrending().subscribe(res => {
      this.images = this.giphyService.processCollection(res.data);
    });
  }
}
